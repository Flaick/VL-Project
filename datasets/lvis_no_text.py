# Copyright (c) Aishwarya Kamath & Nicolas Carion. Licensed under the Apache License 2.0. All Rights Reserved
from pathlib import Path

from transformers import RobertaTokenizerFast

import datasets.transforms as T

from .coco import ModulatedDetection, make_coco_transforms

import torch

class LvisModulatedDetection_no_text(ModulatedDetection):
    def __getitem__(self, idx):
        img, target = super(ModulatedDetection, self).__getitem__(idx)
        image_id = self.ids[idx]
        coco_img = self.coco.loadImgs(image_id)[0]
        caption = str('openmmlab') # force the text input as the dummy text, e.g., openmmlab
        dataset_name = coco_img["dataset_name"] if "dataset_name" in coco_img else None
        target = {"image_id": image_id, "annotations": target, "caption": caption}

        img, target = self.prepare(img, target)
        if self._transforms is not None:
            img, target = self._transforms(img, target)
        target["dataset_name"] = dataset_name
        for extra_key in ["sentence_id", "original_img_id", "original_id", "task_id"]:
            if extra_key in coco_img:
                target[extra_key] = coco_img[extra_key]
        target['positive_map'] = torch.zeros_like(target['positive_map'])
        if "tokens_positive_eval" in coco_img and not self.is_train:
            exit()
            tokenized = self.prepare.tokenizer(caption, return_tensors="pt")
            target["positive_map_eval"] = create_positive_map(tokenized, coco_img["tokens_positive_eval"])
            target["nb_eval"] = len(target["positive_map_eval"])
        # target.keys(): ['boxes', 'labels', 'caption', 'image_id', 'tokens_positive', 'area', 'iscrowd', 'orig_size', 'size', 'positive_map', 'dataset_name', 'original_id']
        return img, target


def build(image_set, args):

    img_dir = Path(args.coco2017_path)
    if args.lvis_subset is None or int(args.lvis_subset) == 100:
        ann_file = Path(args.modulated_lvis_ann_path) / f"finetune_lvis_{image_set}.json"
    else:
        ann_file = Path(args.modulated_lvis_ann_path) / f"finetune_lvis{args.lvis_subset}_{image_set}.json"

    tokenizer = RobertaTokenizerFast.from_pretrained(args.text_encoder_type)
    dataset = LvisModulatedDetection_no_text(
        img_dir,
        ann_file,
        transforms=make_coco_transforms(image_set, cautious=True),
        return_masks=False,
        return_tokens=True,  # args.contrastive_align_loss,
        tokenizer=tokenizer,
    )
    return dataset
