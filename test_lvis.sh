set -x

PARTITION=$1
JOB_NAME=$2
GPUS=${GPUS:-4}
GPUS_PER_NODE=${GPUS_PER_NODE:-8}
CPUS_PER_TASK=${CPUS_PER_TASK:-1}

srun -p ${PARTITION} \
    --job-name=${JOB_NAME} \
    --gres=gpu:${GPUS_PER_NODE} \
    --ntasks=${GPUS} \
    --ntasks-per-node=${GPUS_PER_NODE} \
    --cpus-per-task=${CPUS_PER_TASK} \
    --kill-on-bad-exit=1 \
    ${SRUN_ARGS} \
    python scripts/eval_lvis.py --dataset_config configs/lvis.json --resume https://zenodo.org/record/4721981/files/lvis10_checkpoint.pth --lvis_minival /path/to/lvisminival
